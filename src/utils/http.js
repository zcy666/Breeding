import axios from "axios";
import cookie from "js-cookie";
const http = axios.create({ timeout: 1000 });

http.interceptors.request.use((config) => {
  let token = cookie.get("token");
  if (token) {
    config.headers["Authori-Zation"] = `Bearer ${token}`;
  }

  return config;
});

const fn = (status) => {
  if (status === 410000) {
    cookie.remove("token");
    //状态码的函数调用
    window.location.href = "/login";
  }
};

http.interceptors.response.use(
  (response) => {
    const { data } = response;
    fn(data.status);
    // 状态码的函数调用
    return data;
  },
  (error) => {
    // 三次重试
    //1判断什么情况下会超时：给浏览器设置一个较短的请求超时时间，然后在这个时间范围中没有得到服务器的返回
    //2如何判断错误类型是超时：message中必须是关键字 timeout 还有错误码code="ECONNABORTED"

    if (error.config === "ECONNABORTED" && error.message.includes("timeout")) {
      if (error.config.count) {
        error.config.count++;
      } else {
        error.config.count = 1;
      }
      if (error.config.count > 3) {
        return Promise.reject(error);
      }
      error.config.timeout = error.config.timeout * (error.config.count + 1);
      return http(error.config);
    }

    return Promise.reject(error);
  }
);

export default http;
