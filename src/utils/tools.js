export const deepSearchMenus = (arr, target) => {
  let nArr = [];
  if (!Array.isArray(arr)) {
    return nArr;
  }
  for (let i = 0; i < arr.length; i++) {
    // 如果数据里有与点击的路径相同的路径
    if (arr[i].path === target) {
      // 就将其整条数据存入新数组中
      nArr.push(arr[i]);
      // 然后停止执行
      break;
    }
    // 如果数据一层下还有第二层
    if (arr[i].children) {
      // 函数回调
      let childRes = deepSearchMenus(arr[i].children, target);
      // 如果有相同的数据
      if (childRes.length > 0) {
        // 需要解构 然后存入
        nArr.push(arr[i], ...childRes);
        break;
      }
    }
  }
  return nArr;
};
