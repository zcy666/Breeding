import React from "react";
import { useSelector } from "react-redux";
function authBtn(comp, target) {
  return (props) => {
    const { unique_auth } = useSelector((state) => state.userInfo);
    if (unique_auth.includes(target)) {
      return <>{comp}</>;
    }

    return null;
  };
}

export default authBtn;
