import { Navigate, useLocation } from "react-router-dom";
import cookie from "js-cookie";
import { useSelector } from "react-redux";
import { deepSearchMenus } from "../utils/tools";
const auth = (Comp) => {
  return () => {
    const token = cookie.get("token");
    if (!token) {
      return <Navigate to="/login" />;
    }

    const { menus } = useSelector((state) => state.userInfo);
    const { pathname } = useLocation();
    // console.log(deepSearchMenus(menus, pathname).length);
    if (deepSearchMenus(menus, pathname).length <= 0) {
      return <Navigate to="/403" />;
    }

    return <Comp />;
  };
};
export default auth;
