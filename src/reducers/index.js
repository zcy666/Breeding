import userInfo from "./userInfo";
import { combineReducers } from "redux";
import tags from "./tags";
export default combineReducers({
  userInfo,
  tags,
});
